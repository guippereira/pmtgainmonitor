CXX = g++
CXXFLAGS = -O2 -Wall -fPIC -std=c++11 -fopenmp
INCLUDES = -ILRModel -Ispline123 -Ilib -I/usr/include/eigen3
PYINCLUDES = $(shell python3 -m pybind11 --includes)
ROOTINCLUDES = -I$(shell root-config --incdir)
ROOTFLAGS = $(shell root-config --cflags)
ROOTLIBS = $(shell root-config --libs) -lMinuit2


ArrayMonitor: ArrayMonitor.o
	$(CXX) -o GetGain main.cpp ArrayMonitor.o $(ROOTLIBS) $(ROOTINCLUDES) -fopenmp

ArrayMonitor.o:
	$(CXX) -c $(CXXFLAGS) $(ROOTINCLUDES) ArrayMonitor.h ArrayMonitor.cxx
	
clean:
	rm -f *.o
	rm GetGain
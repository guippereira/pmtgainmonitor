#ifndef Included_ArrayMonitor
#define Included_ArrayMonitor



#include <iostream>
#include "TProfile.h"
#include "TGraphErrors.h"
#include "TVectorD.h"
#include "TF1.h"
#include "TCanvas.h"


struct PMTCalibration {
    double gain;
    double sigma;
    double rchi;
};

struct PMTResults {
    TGraphErrors *linfit;
    TProfile *NProf;
    TGraphErrors *gain_r;
};

class ArrayMonitor {       // The class
    private:
        std::vector<std::vector<double>> data_matrix; //One column for each PMT
        std::vector<double> pmt_pos_x;
        std::vector<double> pmt_pos_y;
        std::vector<double> evt_x;
        std::vector<double> evt_y;

        int nbins = 30;
        double rmax = 720;
        double rmin = 300;
        double n0min = 100;
        int nsensors = -1;
        bool draw = false;
        std::string iname = "Array";
        std::vector<PMTCalibration> calibration; 
        std::vector<PMTResults> results;
        

    public:             // Access specifier
        
        ArrayMonitor(std::vector<double> &pmtx, std::vector<double> &pmty);
        ~ArrayMonitor();
        int nPulses=0;

        double rchi;
        double sigma;
        double gain;
        //int getPMT(){return tnPMTs;};
        void addData(std::vector<double> a, double x, double y);
        std::vector<double> tpmtx;
        std::vector<double> tpmty;
        
        void fitPMT(int pmt);
        void clearData();
        void setProfileBins(int nbins){nbins=nbins;};
        void setMaximumDistance(int rmax){rmax=rmax;};
        void setMinimumDistance(int rmin){rmin=rmin;};
        void setMinimumN0(int n0min){n0min=n0min;};
        void setDataMatrix(std::vector<std::vector<double>> &idata_matrix){data_matrix = idata_matrix;};
        void setDataX(std::vector<double> &ievt_x){evt_x = ievt_x;};
        void setDataY(std::vector<double> &ievt_y){evt_y = ievt_y;};

        void setPulseX(int i, double xx){evt_x[i] = xx;};
        void setPulseY(int i, double yy){evt_y[i] = yy;};

        double getPulseX(int i){return evt_x[i];};
        double getPulseY(int i){return evt_y[i];};

        double getPMTGain(int pmt){return calibration[pmt].gain;};
        double getChi(int pmt){return calibration[pmt].rchi;};
        double getSigma(int pmt){return calibration[pmt].sigma;};
        int getSensorCount(){return nsensors;};
        std::vector<int> getDisabledList();
        PMTResults getPlotResults(int pmt){return results[pmt];};

        void calibrateArray();
        void savePlots();
        void setDraw(bool idraw){draw=idraw;};
        void setDrawName(std::string name){iname=name;};
        
};
#endif

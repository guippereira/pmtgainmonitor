#include "ArrayMonitor.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGaxis.h"

using namespace std;

//Create monitor by inputting XY positioning of arrays
ArrayMonitor::ArrayMonitor(std::vector<double> &pmtx, std::vector<double> &pmty)
{
    pmt_pos_x = pmtx;
    pmt_pos_y = pmty;  
    nPulses=0;
    nsensors = pmtx.size();

    calibration.resize(nsensors,{-1,-1,-1});
    results.resize(nsensors,{nullptr,nullptr,nullptr});
    //outplots.resize(nsensors);
    //std::cout << "MESAGE: " << nsensors << std::endl;
}

//Destructor 
ArrayMonitor::~ArrayMonitor() { 
    data_matrix.clear(); 
    evt_x.clear();
    evt_y.clear();
    rchi=-1;
    sigma=-1;
    gain=-1;
    nPulses=0;
}

//Accumulate data
void ArrayMonitor::addData(std::vector<double> a, double x, double y){

    data_matrix.push_back(a);
    evt_x.push_back(x);
    evt_y.push_back(y);

    nPulses+=1;
}

void ArrayMonitor::fitPMT(int pmt){

    gain = -1;
    rchi = -1;
    sigma = -1;
    
    //Profiles histogram to be used in fitting.
    //The counts in the bins of each profile will be used to calculate the expect value (mu)
    //The gain results will be obtained from a linear fit on the various "mu vs the mean signal"
    TProfile* N0Prof = new TProfile("N0Prof","N0 vs R",nbins,rmin,rmax);
    TProfile* NProf  = new TProfile("NProf","N vs R",nbins,rmin,rmax);
    NProf->GetXaxis()->SetTitle("R (mm)");
    NProf->GetYaxis()->SetTitle("Mean PMT signal [mVns]");
    //long unsigned int npulses = data_matrix.size()
    //std::cout << "Preppgin data" << std::endl;
    for(int i=0; i< (int)data_matrix.size();i++){
        
        double pmt_sig = data_matrix[i][pmt];
        
        //std::cout << "Get PMT position" << std::endl;
        double pmt_x = pmt_pos_x[pmt];
        double pmt_y = pmt_pos_y[pmt];

        //std::cout << "Get event position" << std::endl;
        double evt_xx = evt_x[i];
        double evt_yy = evt_y[i];
        double event_dist = hypot(evt_xx-pmt_x, evt_yy-pmt_y);

        //Cutting by minimum distance to avoid zero count bias
        if(pmt_sig<0 || event_dist<rmin)continue; 

        //Fill profile for zero-signal counts
        if(pmt_sig==0)
            N0Prof->Fill(event_dist, pmt_sig);

         //Fill profile for signal counts
        NProf->Fill(event_dist, pmt_sig);
    }
    
    //std::cout << "Fitting data" << std::endl;
    std::vector<double> mu; // -log(n0/n)
    std::vector<double> mean; //Mean PMT signal in mVns
    std::vector<double> err; //Error as per the poisson statistic (see paper) sqrt((1/n0)-(1/n))
    std::vector<double> err_inverted; //I don't remember why I made this
    std::vector<double> err_0; //Error of mean signal
    std::vector<double> dist; //Could be usefull for plots
     std::vector<double> dist_error; //Could be usefull for plots
    std::vector<double> Qc; //The gain meansurement but directly extract from bin content (debug only)
    std::vector<double> Qc_err; //The gain meansurement but directly extract from bin content (debug only)

    for(int i=0; i<nbins;i++){
        double n0 = N0Prof->GetBinEntries(i); //Get null count
        double n = NProf->GetBinEntries(i); //Get signal count

        if(n0<=n0min)continue;
        double mean_sig = NProf->GetBinContent(i);
        double Vmu = -log(n0/n);
        double Vmu_err = sqrt((1/n0)-(1/n));
        double Vqc = mean_sig/(Vmu); //Qc
        double Vqc_err = Vqc*Vmu_err;//Qc_err

        mu.push_back(Vmu); 
        mean.push_back(mean_sig);
        err.push_back(Vmu_err);
        err_inverted.push_back(1/Vmu_err);
        dist.push_back(NProf->GetXaxis()->GetBinCenter(i));
        err_0.push_back(NProf->GetBinError(i));

        Qc.push_back(Vqc);
        Qc_err.push_back(Vqc_err);

        dist_error.push_back(0);
    }

    //Converting to ROOT vectors
    TVectorD mu_v(mu.size(), &mu[0]);
    TVectorD mean_v(mean.size(), &mean[0]);
    TVectorD err_v(err.size(), &err[0]);
    TVectorD err_0v(err_0.size(), &err_0[0]);
    TVectorD err_inverted_v(err_inverted.size(), &err_inverted[0]);
    TVectorD Qc_v(Qc.size(), &Qc[0]);
    TVectorD Qc_err_v(Qc_err.size(), &Qc_err[0]);
    TVectorD dist_v(dist.size(), &dist[0]);
    TVectorD dist_err_v(dist_error.size(), &dist_error[0]);
    //Linear fit of expected photon vs mean signal
    TGraphErrors *linfit = new TGraphErrors(mean_v, mu_v, err_0v, err_v);
    linfit->GetYaxis()->SetTitle("#mu (phd)");
    linfit->GetXaxis()->SetTitle("Mean PMT signal [mVns]");
    linfit->SetTitle("PMT phd to mVns response");

    TF1 *f2 = new TF1("f2", "[0]*x",  mean_v.Min(), mean_v.Max());
    linfit->Fit(f2,"FQ");
    f2=linfit->GetFunction("f2");

    //Extract parameters from fit
    Double_t invgain = f2->GetParameter(0);
    Double_t fit_error = f2->GetParError(0);
    Double_t tsize = mu_v.GetNoElements();

    //Calculate residuals for Chi2
    std::vector<double> residuals;
    for(int i=0; i<tsize;i++)
        residuals.push_back((mu_v[i]-mean_v[i]*invgain)/err_v[i]);
    TVectorD residuals_v(residuals.size(), &residuals[0]);

    gain = 1/invgain;
    rchi = residuals_v.Sqr().Sum()/(tsize-1);
    sigma = fit_error/(invgain*invgain);

    //Save results in vector
    calibration[pmt].gain = gain;
    calibration[pmt].rchi = rchi;
    calibration[pmt].sigma = sigma;
    //std::cout << draw << std::endl;


    
    if(draw){
        TH1::AddDirectory(kFALSE); 

        TGraphErrors *g = new TGraphErrors(dist_v, Qc_v, dist_err_v, Qc_err_v);
        g->SetTitle("PMT gain vs distance");
        g->GetYaxis()->SetTitle("Q_{c} (mVns/phd)");
        g->GetXaxis()->SetTitle("R (mm)");
        
        results[pmt].linfit = linfit; 
        results[pmt].NProf = NProf; 
        results[pmt].gain_r = g; 

    }
}

//Clear accumulated data
void ArrayMonitor::clearData(){
    data_matrix.clear();
    evt_x.clear();
    evt_y.clear();
};

//Get PMTs with default gain value (-1)
std::vector<int> ArrayMonitor::getDisabledList(){
    std::vector<int> list;
    for(int i=0; i<nsensors; i++){
        std::cout << calibration[i].gain << std::endl;
        if(calibration[i].gain<0){
            list.push_back(i);
        }
    }
    return list;
}

//Fit array in a shot
void ArrayMonitor::calibrateArray(){
  for(int i=0;i<getSensorCount();i++){
      fitPMT(i);
  }
}

//Save plots in vector
void ArrayMonitor::savePlots(){
    for(int i=0;i<getSensorCount();i++){
        TString filename("PMT_"+std::to_string(i)+".pdf");
        TString title("PMT "+std::to_string(i));
        TString name(iname+"PMT"+std::to_string(i));

        TCanvas *cnew = new TCanvas(name,title,800,600);

        cnew->Divide(2, 2);
        cnew->cd(1);
        getPlotResults(i).NProf->Draw();

        cnew->cd(2);
        getPlotResults(i).gain_r->Draw();

        cnew->cd(3);
        getPlotResults(i).linfit->Draw();        

        cnew->SaveAs(filename);
    }
}
